package com.evgeny.ubugtrack;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by estrelnikov on 10.03.2017.
 */

@RunWith(AndroidJUnit4.class)
public class LoginFragmentEspressoTest {
    @Rule
    public ActivityTestRule <MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testLogin() {
        onView(withId(R.id.tokenEditText)).perform(typeText("6b285a606bf21bedd4433d5f8915f89853442843ed8fc9e69e1c14bd306fc21a"), closeSoftKeyboard());
        onView(withId(R.id.applyButton)).perform(click());
    }

    @Test
    public void testLogout() {
        onView(withId(R.id.exit_list_project)).perform(click());
    }
}
