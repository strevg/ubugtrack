package com.evgeny.ubugtrack;

import com.evgeny.ubugtrack.utils.StringHelper;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testGetCreatedDateString() throws Exception {
        assertEquals("Get created date string failed","2017-10-29", StringHelper.getCreatedDateString("20171029"));
    }

    @Test
    public void testGetCreatedDate() throws Exception {
        assertEquals("Get created date failed", StringHelper.getCreatedDate("2017-11-27"), StringHelper.getCreatedDate("2017-11-27"));
    }
}