package com.evgeny.ubugtrack.views.adapters;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.databinding.ViewHolderTicketBinding;
import com.evgeny.ubugtrack.models.TicketModel;
import com.evgeny.ubugtrack.models.TicketUpdateModel;
import com.evgeny.ubugtrack.utils.Consts;
import com.evgeny.ubugtrack.views.menu.Label;
import com.evgeny.ubugtrack.views.menu.LabelColored;
import com.evgeny.ubugtrack.views.menu.LabelWhite;
import com.evgeny.ubugtrack.views.viewholders.TicketViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by estrelnikov on 30.11.2016.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketViewHolder> {
    List<TicketModel> ticketList;
    ProjectAdapter.IDClickListener onClickListener;
    Context context;

    public TicketAdapter(Context context, ArrayList<TicketModel> list) {
        this.context = context;
        this.ticketList = list;
    }

    public void setOnClickListener(ProjectAdapter.IDClickListener listener) {
        onClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    @Override
    public TicketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewHolderTicketBinding binding = ViewHolderTicketBinding.inflate(inflater, parent, false);
        return new TicketViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(TicketViewHolder holder, final int position) {
        holder.binding.setTicket(ticketList.get(position));
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener!=null)
                    onClickListener.onClick(ticketList.get(position).id, null);
            }
        });
    }

    public void onAddAll(List<TicketModel> list) {
        ticketList = list;
        notifyDataSetChanged();
    }

    public TicketModel onAddNewTicket(List<TicketModel> list, TicketUpdateModel ticket) {
        for (TicketModel ticketModel : list) {
            if (ticket.isEquals(ticketModel) && !ticketList.contains(ticketModel)) {
                ticketList.add(0,ticketModel);
                notifyItemInserted(0);
                return ticketModel;
            }
        }
        return null;
    }

    public void onUpdateTicket(final TicketUpdateModel model) {
        int pos=0;
        for (TicketModel ticketModel : ticketList) {
            if (ticketModel.id == model.id) {
                if (model.status!=null)
                    ticketModel.status = model.status;
                if (model.description!=null)
                    ticketModel.description = model.description;
                if (model.priority!=null)
                    ticketModel.priority = model.priority;
                if (model.title!=null)
                    ticketModel.title = model.title;
                if (model.external_id!=null)
                    ticketModel.external = model.external_id;
                notifyItemChanged(pos);
                return;
            }
            pos++;
        }
    }

    //-------------- sort methods ----------------
    public void onSortTimeUpdated(boolean ascending) {
        for (int i=ticketList.size()-1; i>0; i--) {
            for (int j=0; j<i; j++) {
                if (ascending) {
                    if ((ticketList.get(j).timeUpdated.compareTo(ticketList.get(j + 1).timeUpdated) < 0) ||
                            (ticketList.get(j).timeUpdated.compareTo(ticketList.get(j + 1).timeUpdated) == 0 && ticketList.get(j).id < ticketList.get(j+1).id)) {
                        TicketModel temp = ticketList.get(j);
                        ticketList.set(j, ticketList.get(j + 1));
                        ticketList.set(j + 1, temp);
                    }
                } else {
                    if (ticketList.get(j).timeUpdated.compareTo(ticketList.get(j + 1).timeUpdated) > 0 ||
                            (ticketList.get(j).timeUpdated.compareTo(ticketList.get(j + 1).timeUpdated) == 0 && ticketList.get(j).id < ticketList.get(j+1).id)) {
                        TicketModel temp = ticketList.get(j);
                        ticketList.set(j, ticketList.get(j + 1));
                        ticketList.set(j + 1, temp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public void onSortTimeCreated(boolean ascending) {
        for (int i=ticketList.size()-1; i>0; i--) {
            for (int j=0; j<i; j++) {
                if (ascending) {
                    if ((ticketList.get(j).dateCreated.compareTo(ticketList.get(j + 1).dateCreated) < 0) ||
                            (ticketList.get(j).dateCreated.compareTo(ticketList.get(j + 1).dateCreated) == 0 && ticketList.get(j).id < ticketList.get(j+1).id)) {
                        TicketModel temp = ticketList.get(j);
                        ticketList.set(j, ticketList.get(j + 1));
                        ticketList.set(j + 1, temp);
                    }
                } else {
                    if (ticketList.get(j).dateCreated.compareTo(ticketList.get(j + 1).dateCreated) > 0 ||
                            (ticketList.get(j).dateCreated.compareTo(ticketList.get(j + 1).dateCreated) == 0 && ticketList.get(j).id < ticketList.get(j+1).id)) {
                        TicketModel temp = ticketList.get(j);
                        ticketList.set(j, ticketList.get(j + 1));
                        ticketList.set(j + 1, temp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    //------------- binding methods ----------------
    @BindingAdapter("bind:status")
    public static void setStatus(LabelColored label, int status){
        label.setIndex(status);
        label.setLabelText((label.getContext().getResources().getStringArray(R.array.status_text_array))[status]);
        label.setBackgroundColor((label.getContext().getResources().getIntArray(R.array.status_color_array))[status]);
    }

    @BindingAdapter("bind:priority")
    public static void setPriority(LabelWhite label, int priority){
        label.setIndex(priority);
        label.setLabelText((label.getContext().getResources().getStringArray(R.array.priority_text_array))[priority]);
    }
}
