package com.evgeny.ubugtrack.views.menu;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.evgeny.ubugtrack.R;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuCustomItem;
import com.shehabic.droppy.DroppyMenuItem;
import com.shehabic.droppy.DroppyMenuPopup;

/**
 * Created by Evgeny on 27.11.2016.
 */

public class PriorityPopupMenu {
    DroppyMenuPopup.Builder droppyBuilder;
    public PriorityPopupMenu(Context context, View view, DroppyClickCallbackInterface listener){
        droppyBuilder = new DroppyMenuPopup.Builder(context, view);
        for (String priority : context.getResources().getStringArray(R.array.priority_text_array)) {
            LabelWhite label = new LabelWhite(context, priority);
            DroppyMenuCustomItem menuItem = new DroppyMenuCustomItem(label);
            menuItem.setClickable(true);
            droppyBuilder.addMenuItem(menuItem);
        }
        setListener(listener);
        droppyBuilder.build();
    }

    public void setListener(DroppyClickCallbackInterface listener) {
        droppyBuilder.setOnClick(listener);
    }
}
