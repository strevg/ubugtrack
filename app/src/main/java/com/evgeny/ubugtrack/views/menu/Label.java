package com.evgeny.ubugtrack.views.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evgeny.ubugtrack.R;

/**
 * Created by Evgeny on 27.11.2016.
 */

public abstract class Label extends LinearLayout {
    TextView labelText;
    int labelIndex;

    public Label(Context context) {
        super(context);
        initViews();
    }

    public Label(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributeSet(context, attrs, 0);
    }

    void initAttributeSet(Context context, AttributeSet attrs, int defStyle) {
        initViews();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Label, defStyle, 0);
        String titleText = a.getString(R.styleable.Label_labelText);
        int color = a.getColor(R.styleable.Label_labelColor, 0);
        boolean center = a.getBoolean(R.styleable.Label_textAlignCenter, false);
        if (titleText!=null)
            setLabelText(titleText);
        if (color!=0)
            setBackgroundColor(color);
        if (center)
            labelText.setGravity(Gravity.CENTER);
        a.recycle();
    }

    abstract void initViews();

    public void setLabelText(String text) {
        labelText.setText(text);
    }

    public void setBackgroundColor(int color) {
        ((GradientDrawable)labelText.getBackground().mutate()).setColor(color);
    }

    public void setIndex(int index) {
        this.labelIndex = index;
    }

    public int getIndex() {
        return labelIndex;
    }
}
