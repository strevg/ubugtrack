package com.evgeny.ubugtrack.views.menu;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.utils.Consts;

/**
 * Created by Evgeny on 27.11.2016.
 */

public class LabelColored extends Label {
    public LabelColored(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LabelColored(Context context, String text, int color) {
        super(context);
        setBackgroundColor(color);
        setLabelText(text);
    }

    void initViews() {
        inflate(getContext(), R.layout.label_colored, this);
        labelText = (TextView)findViewById(R.id.textLabel);
    }
}
