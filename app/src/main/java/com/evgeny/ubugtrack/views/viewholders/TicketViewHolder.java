package com.evgeny.ubugtrack.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.evgeny.ubugtrack.databinding.ViewHolderTicketBinding;

/**
 * Created by estrelnikov on 28.11.2016.
 */

public class TicketViewHolder extends RecyclerView.ViewHolder {
    public ViewHolderTicketBinding binding;
    View view;

    public TicketViewHolder(View v) {
        super(v);
        view = v;
        binding = DataBindingUtil.bind(v);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        view.setOnClickListener(listener);
    }
}
