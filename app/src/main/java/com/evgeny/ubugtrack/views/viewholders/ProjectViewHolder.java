package com.evgeny.ubugtrack.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.evgeny.ubugtrack.databinding.ViewHolderProjectBinding;

/**
 * Created by estrelnikov on 09.01.2017.
 */

public class ProjectViewHolder extends RecyclerView.ViewHolder {
    public ViewHolderProjectBinding binding;
    View view;

    public ProjectViewHolder(View v) {
        super(v);
        view = v;
        binding = DataBindingUtil.bind(v);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        view.setOnClickListener(listener);
    }
}
