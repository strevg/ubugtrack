package com.evgeny.ubugtrack.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.utils.Consts;

/**
 * Created by Evgeny on 27.11.2016.
 */

public class LabelWhite extends Label {
    public LabelWhite(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LabelWhite(Context context, String text) {
        super(context);
        setLabelText(text);
    }

    void initViews() {
        inflate(getContext(), R.layout.label_white, this);
        labelText = (TextView)findViewById(R.id.textLabel);
    }
}
