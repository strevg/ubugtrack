package com.evgeny.ubugtrack.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.ubugtrack.databinding.ViewHolderProjectBinding;
import com.evgeny.ubugtrack.models.ProjectModel;
import com.evgeny.ubugtrack.views.viewholders.ProjectViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by estrelnikov on 09.01.2017.
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectViewHolder> {
    ArrayList<ProjectModel> projectsList;
    IDClickListener onClickListener;
    Context context;

    public ProjectAdapter(Context context) {
        this.context = context;
        this.projectsList = new ArrayList<>();
    }

    public void setOnClickListener(IDClickListener listener) {
        onClickListener = listener;
    }

    @Override
    public void onBindViewHolder(ProjectViewHolder holder, final int position) {
        holder.binding.setProject(projectsList.get(position));
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener!=null)
                    onClickListener.onClick(projectsList.get(position).id, projectsList.get(position).name);
            }
        });
    }

    @Override
    public ProjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewHolderProjectBinding projectViewLayoutBinding = ViewHolderProjectBinding.inflate(inflater, parent, false);
        return new ProjectViewHolder(projectViewLayoutBinding.getRoot());
    }

    @Override
    public int getItemCount() {
        return projectsList.size();
    }

    public void onAddAll(List<ProjectModel> list) {
        projectsList.clear();
        projectsList.addAll(list);
        notifyDataSetChanged();
    }

    public interface IDClickListener {
        void onClick(long id, String name);
    }
}
