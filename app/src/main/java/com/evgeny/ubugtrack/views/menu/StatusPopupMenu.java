package com.evgeny.ubugtrack.views.menu;

import android.content.Context;
import android.view.View;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.utils.Consts;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuCustomItem;
import com.shehabic.droppy.DroppyMenuPopup;

import java.util.ArrayList;

/**
 * Created by Evgeny on 27.11.2016.
 */

public class StatusPopupMenu {
    DroppyMenuPopup.Builder droppyBuilder;
    public StatusPopupMenu(Context context, View view, DroppyClickCallbackInterface listener) {
        droppyBuilder = new DroppyMenuPopup.Builder(context, view);
        int[] colorArray = context.getResources().getIntArray(R.array.status_color_array);
        int index = 0;
        for (String text : context.getResources().getStringArray(R.array.status_text_array)) {
            LabelColored label = new LabelColored(context, text, colorArray[index]);
            DroppyMenuCustomItem menuItem = new DroppyMenuCustomItem(label);
            menuItem.setClickable(true);
            droppyBuilder.addMenuItem(menuItem);
            index++;
        }
        setListener(listener);
        droppyBuilder.build();
    }

    public void setListener(DroppyClickCallbackInterface listener) {
        droppyBuilder.setOnClick(listener);
    }
}
