package com.evgeny.ubugtrack.network;

import com.evgeny.ubugtrack.models.ProjectListModel;
import com.evgeny.ubugtrack.models.ResponseModel;
import com.evgeny.ubugtrack.models.TicketListModel;
import com.evgeny.ubugtrack.models.TicketUpdateModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Evgeny on 08.01.2017.
 */

public interface UbugtrackApi {
    @GET("projects")
    Call<ProjectListModel> getProjectsList();

    @GET("project/{project_id}/tickets")
    Call<TicketListModel> getTicketsList(
            @Path("project_id") String project_id,
            @QueryMap Map<String, String> options);

    @POST("project/{project_id}/ticket/{ticket_id}/update")
    Call<ResponseModel> updateTicket(@Path("project_id") String project_id,
                                     @Path("ticket_id") String ticket_id,
                                     @Body TicketUpdateModel updateTicket);

    @POST("project/{project_id}/newticket")
    Call<ResponseModel> createNewTicket(@Path("project_id") String project_id,
                                        @Body TicketUpdateModel updateTicket);
}
