package com.evgeny.ubugtrack.network;

import android.support.annotation.NonNull;

import com.evgeny.ubugtrack.utils.Consts;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by estrelnikov on 21.02.2017.
 */

public class ApiFactory {
    private static final OkHttpClient CLIENT = new OkHttpClient();

    @NonNull
    public static UbugtrackApi getUbugtrackApi() {
        return getRetrofit().create(UbugtrackApi.class);
    }

    @NonNull
    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://ubugtrack.com/api/" + Consts.API_KEY + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(CLIENT)
                .build();
    }
}
