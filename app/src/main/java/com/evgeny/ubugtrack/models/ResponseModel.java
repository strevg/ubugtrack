package com.evgeny.ubugtrack.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by estrelnikov on 10.01.2017.
 */

public class ResponseModel {
    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;
}
