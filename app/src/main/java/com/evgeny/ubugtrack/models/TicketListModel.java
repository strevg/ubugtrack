package com.evgeny.ubugtrack.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by estrelnikov on 09.01.2017.
 */

public class TicketListModel extends RealmObject{
    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;
    @SerializedName("tickets")
    public RealmList<TicketModel> ticketsList;
}
