package com.evgeny.ubugtrack.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by Evgeny on 08.01.2017.
 */

public class ProjectListModel extends RealmObject{
    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;
    @SerializedName("projects")
    public RealmList<ProjectModel> projectList;
}
