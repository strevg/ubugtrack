package com.evgeny.ubugtrack.models;

/**
 * Created by estrelnikov on 10.01.2017.
 */

public class TicketUpdateModel {
    public long id=-1;
    public String title;
    public String description;
    public Boolean markdown;
    public Integer status;
    public Integer priority;
    public String external_id;
    public TicketUpdateModel() {
        this.title = null;
        this.description = null;
        this.external_id = null;
        this.markdown = null;
        this.status = null;
        this.priority = null;
    }

    public boolean isEquals(TicketModel ticketModel) {
        if ((title!=null && title.equals(ticketModel.title) || title==null) &&
                (description!=null && description.equals(ticketModel.description) || description==null) &&
                (markdown!=null && markdown==ticketModel.markdown || markdown==null) &&
                (status!=null && status == ticketModel.status || status==null) &&
                (priority!=null && priority==ticketModel.priority || priority==null) &&
                (external_id!=null && external_id.equals(ticketModel.external) || external_id==null))
            return true;
        return false;
    }
}
