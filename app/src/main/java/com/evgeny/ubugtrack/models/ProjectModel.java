package com.evgeny.ubugtrack.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmObject;

/**
 * Created by estrelnikov on 09.01.2017.
 */

public class ProjectModel extends RealmObject{
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("user_owner_id")
    public long user_owner_id;

    public ProjectModel(){}

    public String getName() {
        return name;
    }
}
