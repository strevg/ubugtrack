package com.evgeny.ubugtrack.models;

import com.evgeny.ubugtrack.utils.StringHelper;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by estrelnikov on 28.11.2016.
 */

public class TicketModel extends RealmObject{
    @PrimaryKey
    @SerializedName("id")
    public long id;
    @SerializedName("date_created")
    public String date_created;
    @SerializedName("time_updated")
    public int time_updated;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("markdown")
    public boolean markdown;
    @SerializedName("status")
    public int status;
    @SerializedName("priority")
    public int priority;
    @SerializedName("external_id")
    public String external;
    @SerializedName("assigned_displayname")
    public String assigned;

    public String dateCreatedString;
    public String timeUpdatedString;
    public Date dateCreated;
    public Date timeUpdated;

    public TicketModel(){}

    public TicketModel(int id, String created, String updated, String title, String description,
                       int status, int priority, String external, String assigned, boolean markdown) {
        this.id = id;
        this.dateCreatedString = created;
        this.timeUpdatedString = updated;
        this.title = title;
        this.description = description;
        this.status = status;
        this.priority = priority;
        this.external = external;
        this.assigned = assigned;
        this.markdown = markdown;

        this.timeUpdated = StringHelper.getUpdatedDate(updated);
        this.dateCreated = StringHelper.getCreatedDate(StringHelper.getCreatedDateString(created));
    }

    public TicketModel(TicketModel model) {
        this.id = model.id;
        this.dateCreatedString = model.date_created;
        this.timeUpdatedString = model.time_updated+"";
        this.title = model.title;
        this.description = model.description;
        this.status = model.status;
        this.priority = model.priority;
        this.external = model.external;
        this.assigned = model.assigned;
        this.markdown = model.markdown;

        this.timeUpdated = StringHelper.getUpdatedDate(model.time_updated+"");
        this.dateCreated = StringHelper.getCreatedDate(StringHelper.getCreatedDateString(model.date_created));
    }

    public String getDate_created() {
        return date_created;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getDateCreatedString() {
        return dateCreatedString;
    }

    public String getDescription() {
        return description;
    }

    public String getExternal() {
        return external;
    }

    public boolean isMarkdown() {
        return markdown;
    }

    public long getId() {
        return id;
    }

    public int getPriority() {
        return priority;
    }

    public int getStatus() {
        return status;
    }

    public int getTime_updated() {
        return time_updated;
    }

    public Date getTimeUpdated() {
        return timeUpdated;
    }

    public String getTimeUpdatedString() {
        return timeUpdatedString;
    }

    public String getTitle() {
        return title;
    }

    public String getAssigned() {
        return assigned;
    }
}
