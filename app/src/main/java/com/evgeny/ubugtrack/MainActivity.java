package com.evgeny.ubugtrack;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.evgeny.ubugtrack.fragments.FragmentCreateTicket;
import com.evgeny.ubugtrack.fragments.FragmentListProjects;
import com.evgeny.ubugtrack.fragments.FragmentListTickets;
import com.evgeny.ubugtrack.fragments.FragmentLogin;
import com.evgeny.ubugtrack.fragments.FragmentTicket;
import com.evgeny.ubugtrack.utils.Consts;

import java.lang.ref.WeakReference;


public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {
    private static WeakReference<MainActivity> instance = null;
    ProgressBar progressBar;
    Consts.FragmentType currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = new WeakReference<>(this);
        initViews();

        if (savedInstanceState==null) {
            Fragment currentFragment;
            if (Consts.HAS_API_KEY)
                currentFragment = new FragmentListProjects();
            else
                currentFragment = new FragmentLogin();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.contentFragment, currentFragment);
            fragmentTransaction.commit();
        } else {
            getSupportActionBar().setTitle(savedInstanceState.getString("title"));
        }
    }

    void initViews() {
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        setSupportActionBar(mActionBarToolbar);
        getFragmentManager().addOnBackStackChangedListener(this);
        shouldDisplayHomeUp();
    }

    public void showFragment(Consts.FragmentType type, Bundle bundle) {
        currentFragment = type;
        Fragment fragment = new Fragment();
        switch (type) {
            case TICKETS_LIST:
                fragment = new FragmentListTickets();
                break;
            case TICKET:
                fragment = new FragmentTicket();
                break;
            case CREATE_TICKET:
                fragment = new FragmentCreateTicket();
                break;
        }
        if (bundle!=null)
            fragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.contentFragment, fragment, type.name());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void openFragment(Consts.FragmentType type) {
        Fragment fragment = new Fragment();
        switch (type) {
            case PROJECTS_LIST:
                fragment = new FragmentListProjects();
                break;
            case LOGIN:
                fragment = new FragmentLogin();
                break;
        }
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentFragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("saveInstanceState", true);
        outState.putString("title", getSupportActionBar().getTitle().toString());
    }

    @Override
    public void onBackPressed() {
        if (currentFragment == Consts.FragmentType.TICKET) {
            FragmentTicket ticket = ((FragmentTicket)getFragmentManager().findFragmentByTag(currentFragment.name()));
            if (ticket!=null && ticket.isEditState()) {
                ticket.changeEditable();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp(){
        boolean canBack = getFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canBack);
    }

    public void showProgressBar(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public static MainActivity getInstance() {
        if (instance == null) {
            return null;
        }
        MainActivity activity = instance.get();
        return activity;
    }
}
