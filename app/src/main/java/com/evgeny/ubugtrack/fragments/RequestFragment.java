package com.evgeny.ubugtrack.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.network.ApiFactory;
import com.evgeny.ubugtrack.network.UbugtrackApi;
import com.evgeny.ubugtrack.utils.Consts;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.realm.Realm;

/**
 * Created by estrelnikov on 28.02.2017.
 */

public abstract class RequestFragment extends Fragment {
    Context context;
    UbugtrackApi ubugtrackApi;
    Realm realm;

    int resourceLayout;
    static boolean isLoading = false;

    public RequestFragment(int layout) {
        this.resourceLayout = layout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ubugtrackApi = ApiFactory.getUbugtrackApi();
        realm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(resourceLayout, container, false);
        this.context = view.getContext();
        initViews(view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        realm.close();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
        onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isLoading", isLoading);
    }

    void showProgressBar(boolean show) {
        MainActivity.getInstance().showProgressBar(show);
    }

    //----------- storage methods -----------
    void onSaveInRealm() {
        realm = Realm.getDefaultInstance();
    }

    //---------- abstract methods need override ------------
    abstract void initViews(View view);

    abstract void onRestoreInstanceState(Bundle savedInstanceState);
}
