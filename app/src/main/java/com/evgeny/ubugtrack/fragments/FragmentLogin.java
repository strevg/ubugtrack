package com.evgeny.ubugtrack.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.utils.Consts;

/**
 * Created by Evgeny on 20.01.2017.
 */

public class FragmentLogin extends Fragment implements View.OnClickListener{
    static final String URI_TOKEN = "https://ubugtrack.com/?page=api_help#section-api-key";

    Context context;
    Button getTokenButton, applyTokenButton;
    EditText tokenEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initViews(view);
        return view;
    }

    void initViews(View view) {
        context = view.getContext();
        getTokenButton = (Button)view.findViewById(R.id.getTokenButton);
        applyTokenButton = (Button)view.findViewById(R.id.applyButton);
        tokenEditText = (EditText)view.findViewById(R.id.tokenEditText);
        getTokenButton.setOnClickListener(this);
        applyTokenButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.applyButton:
                applyKeyApi();
                break;
            case R.id.getTokenButton:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URI_TOKEN));
                startActivity(browserIntent);
                break;
        }
    }

    void applyKeyApi() {
        if (tokenEditText.getText().length()>0) {
            Consts.HAS_API_KEY = true;
            Consts.API_KEY = tokenEditText.getText().toString();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            sharedPreferences.edit().putString(Consts.API_KEY_STR, Consts.API_KEY).commit();
            sharedPreferences.edit().putBoolean(Consts.HAS_API_KEY_STR, Consts.HAS_API_KEY).commit();

            MainActivity.getInstance().openFragment(Consts.FragmentType.PROJECTS_LIST);
        }
    }
}
