package com.evgeny.ubugtrack.fragments;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.databinding.FragmentTicketBinding;
import com.evgeny.ubugtrack.models.ResponseModel;
import com.evgeny.ubugtrack.models.TicketModel;
import com.evgeny.ubugtrack.models.TicketUpdateModel;
import com.evgeny.ubugtrack.network.ApiFactory;
import com.evgeny.ubugtrack.network.UbugtrackApi;
import com.evgeny.ubugtrack.utils.Consts;
import com.evgeny.ubugtrack.views.menu.PriorityPopupMenu;
import com.evgeny.ubugtrack.views.menu.StatusPopupMenu;
import com.shehabic.droppy.DroppyClickCallbackInterface;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by estrelnikov on 05.12.2016.
 */

public class FragmentTicket extends Fragment {
    FragmentTicketBinding binding;
    PriorityPopupMenu priorityPopupMenu;
    StatusPopupMenu statusMenu;
    Button saveChangedButton;
    boolean isEditState = true;

    TicketModel currentTicket;
    long idTicket;
    long idProject;

    UbugtrackApi ubugtrackApi;
    Realm realm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ubugtrackApi = ApiFactory.getUbugtrackApi();
        realm = Realm.getDefaultInstance();
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle!=null) {
            idTicket = bundle.getLong("idTicket");
            idProject = bundle.getLong("idProject");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ticket, container, false);
        View rootView = binding.getRoot();
        initViews(rootView);
        return rootView;
    }

    void initViews(View view) {
        saveChangedButton = (Button)view.findViewById(R.id.saveChangeButton);
        saveChangedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeEditable();
                updateChanges();
            }
        });
        statusMenu = new StatusPopupMenu(getActivity(), binding.statusTicket, new DroppyClickCallbackInterface() {
            @Override
            public void call(View v, int id) {
                int[] colorArray = v.getContext().getResources().getIntArray(R.array.status_color_array);
                String[] str = v.getContext().getResources().getStringArray(R.array.status_text_array);
                binding.statusTicket.setLabelText(str[id]);
                binding.statusTicket.setBackgroundColor(colorArray[id]);
                binding.statusTicket.setIndex(id);
            }
        });
        priorityPopupMenu = new PriorityPopupMenu(getActivity(), binding.priorityTicket, new DroppyClickCallbackInterface() {
            @Override
            public void call(View v, int id) {
                String[] str = v.getContext().getResources().getStringArray(R.array.priority_text_array);
                binding.priorityTicket.setLabelText(str[id]);
                binding.priorityTicket.setIndex(id);
            }
        });
        binding.titleTicket.setEnabled(false);
        binding.descriptionTicket.setEnabled(false);
        binding.priorityTicket.setClickable(false);
        binding.statusTicket.setClickable(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currentTicket = realm.where(TicketModel.class).equalTo("id", idTicket).findFirst();
        binding.setTicket(currentTicket);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.ticket_fragment_menu, menu);
        menu.findItem(R.id.edit_ticket_menu).setVisible(isEditState);
        menu.findItem(R.id.cancel_ticket_menu).setVisible(!isEditState);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_ticket_menu:
                changeEditable();
                return true;
            case R.id.cancel_ticket_menu:
                changeEditable();
                cancelChanges();
                return true;
        }
        return false;
    }

    public void changeEditable() {
        isEditState =! isEditState;
        // кнопки сохранение и изменения
        saveChangedButton.setVisibility(!isEditState?View.VISIBLE:View.GONE);
        getActivity().invalidateOptionsMenu();
        // поля
        binding.titleTicket.setEnabled(!isEditState);
        binding.descriptionTicket.setEnabled(!isEditState);
        binding.priorityTicket.setClickable(!isEditState);
        binding.statusTicket.setClickable(!isEditState);
    }

    void cancelChanges() {
        binding.setTicket(currentTicket);
    }

    public boolean isEditState() {
        return !isEditState;
    }
    //------------ networks --------------
    void updateChanges() {
        Call<ResponseModel> ticketUpdate = ubugtrackApi.updateTicket(String.valueOf(idProject), String.valueOf(idTicket), getChanges());
        ticketUpdate.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.body().success) {
                    Toast.makeText(MainActivity.getInstance(), "Ticket updated", Toast.LENGTH_LONG).show();
                    EventBus.getDefault().postSticky(getChanges());
                    saveInRealm();
                }
            }
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e(Consts.TAG, "onFailure " + t.toString());
                Toast.makeText(MainActivity.getInstance(), "Error. Try again!", Toast.LENGTH_LONG).show();
            }
        });
    }

    //----------- storage methods -----------
    void saveInRealm() {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(getUpdatedTicket());
        realm.commitTransaction();
        realm.close();
    }

    //------------- GET-SET methods --------------
    TicketUpdateModel getChanges() {
        TicketUpdateModel updateModel = new TicketUpdateModel();
        updateModel.id = idTicket;
        if (!currentTicket.title.equals(binding.titleTicket.getText().toString()))
            updateModel.title = binding.titleTicket.getText().toString();
        if (!currentTicket.description.equals(binding.descriptionTicket.getText().toString()))
            updateModel.description = binding.descriptionTicket.getText().toString();
        if (currentTicket.priority!=binding.priorityTicket.getIndex())
            updateModel.priority = binding.priorityTicket.getIndex();
        if (currentTicket.status!=binding.statusTicket.getIndex())
            updateModel.status = binding.statusTicket.getIndex();
        return updateModel;
    }

    TicketModel getUpdatedTicket() {
        TicketModel updateModel = currentTicket;
        if (!currentTicket.title.equals(binding.titleTicket.getText().toString()))
            updateModel.title = binding.titleTicket.getText().toString();
        if (!currentTicket.description.equals(binding.descriptionTicket.getText().toString()))
            updateModel.description = binding.descriptionTicket.getText().toString();
        if (currentTicket.priority!=binding.priorityTicket.getIndex())
            updateModel.priority = binding.priorityTicket.getIndex();
        if (currentTicket.status!=binding.statusTicket.getIndex())
            updateModel.status = binding.statusTicket.getIndex();
        return updateModel;
    }
}
