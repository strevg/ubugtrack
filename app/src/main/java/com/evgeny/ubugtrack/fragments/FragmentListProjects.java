package com.evgeny.ubugtrack.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.models.ProjectListModel;
import com.evgeny.ubugtrack.models.ProjectModel;
import com.evgeny.ubugtrack.utils.Consts;
import com.evgeny.ubugtrack.views.adapters.ProjectAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Evgeny on 09.01.2017.
 */

public class FragmentListProjects extends RequestFragment implements ProjectAdapter.IDClickListener {
    ProjectAdapter adapter;

    public FragmentListProjects() {
        super(R.layout.fragment_list_projects);
    }

    void initViews(View rootView) {
        setHasOptionsMenu(true);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewProject);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new ProjectAdapter(rootView.getContext());
        adapter.setOnClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(long id, String name) {
        Bundle bundle = new Bundle();
        bundle.putLong("idProject", id);
        bundle.putString("nameProject", name);
        MainActivity.getInstance().showFragment(Consts.FragmentType.TICKETS_LIST, bundle);
    }

    @Override
    void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            isLoading = savedInstanceState.getBoolean("isLoading");
            if (!isLoading)
                loadFromRealm();
        } else {
            downloadProjectsList();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.list_projects_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.exit_list_project) {
            logOutApp();
            return true;
        }
        return false;
    }

    // -------------- network methods ---------------
    void downloadProjectsList() {
        isLoading = true;
        showProgressBar(true);
        Call<ProjectListModel> call = ubugtrackApi.getProjectsList();
        call.enqueue(new Callback<ProjectListModel>() {
            @Override
            public void onResponse(Call<ProjectListModel> call, final Response<ProjectListModel> response) {
                isLoading = false;
                showProgressBar(false);
                onSaveInRealm(response.body().projectList);
                EventBus.getDefault().postSticky(response.body());
            }
            @Override
            public void onFailure(Call<ProjectListModel> call, Throwable t) {
                isLoading = false;
                showProgressBar(false);
                Log.i(Consts.TAG, "onFailure " + t.getMessage());
            }
        });
    }

    //----------- eventbus methods ----------
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResponseSuccess(ProjectListModel data) {
        if (data.projectList!=null)
            adapter.onAddAll(data.projectList);
        EventBus.getDefault().removeStickyEvent(data);
    }

    //----------- storage methods -----------
    void onSaveInRealm(final List<ProjectModel> list) {
        super.onSaveInRealm();
        realm.beginTransaction();
        realm.delete(ProjectModel.class);
        realm.copyToRealmOrUpdate(list);
        realm.commitTransaction();
    }

    void loadFromRealm() {
        List<ProjectModel> list = realm.where(ProjectModel.class).findAll();
        adapter.onAddAll(list);
    }

    void logOutApp() {
        Consts.HAS_API_KEY = false;
        Consts.API_KEY = null;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().putString(Consts.API_KEY_STR, Consts.API_KEY).commit();
        sharedPreferences.edit().putBoolean(Consts.HAS_API_KEY_STR, Consts.HAS_API_KEY).commit();

        MainActivity.getInstance().openFragment(Consts.FragmentType.LOGIN);
    }
}
