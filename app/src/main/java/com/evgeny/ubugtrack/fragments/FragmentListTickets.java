package com.evgeny.ubugtrack.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.models.ProjectModel;
import com.evgeny.ubugtrack.models.TicketListModel;
import com.evgeny.ubugtrack.models.TicketModel;
import com.evgeny.ubugtrack.models.TicketUpdateModel;
import com.evgeny.ubugtrack.network.ApiFactory;
import com.evgeny.ubugtrack.network.UbugtrackApi;
import com.evgeny.ubugtrack.utils.Consts;
import com.evgeny.ubugtrack.views.adapters.ProjectAdapter;
import com.evgeny.ubugtrack.views.adapters.TicketAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by estrelnikov on 30.11.2016.
 */

public class FragmentListTickets extends RequestFragment implements ProjectAdapter.IDClickListener {
    RecyclerView recycleView;
    TicketAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    FloatingActionButton createNewButton;
    int filterStatus = -1;
    TicketUpdateModel createdTicket;

    long idProject;

    public FragmentListTickets() {
        super(R.layout.fragment_list_tickets);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle!=null) {
            idProject = bundle.getLong("idProject");
            MainActivity.getInstance().getSupportActionBar().setTitle(bundle.getString("nameProject"));
        }
    }

    void initViews(View view) {
        filterStatus = -1;

        recycleView = (RecyclerView)view.findViewById(R.id.recycleView);
        recycleView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(view.getContext());
        recycleView.setLayoutManager(layoutManager);

        adapter = new TicketAdapter(view.getContext(), new ArrayList<TicketModel>());
        adapter.setOnClickListener(this);
        recycleView.setAdapter(adapter);

        createNewButton = (FloatingActionButton)view.findViewById(R.id.createNewFloatButton);
        createNewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("idProject", idProject);
                MainActivity.getInstance().showFragment(Consts.FragmentType.CREATE_TICKET, bundle);
            }
        });

        setHasOptionsMenu(true);

        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.status_filter_text_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(filterStatus!=position-1) {
                    filterStatus = position - 1;
                    showProgressBar(true);
                    downloadListTickets();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        MainActivity.getInstance().getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    @Override
    void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            isLoading = savedInstanceState.getBoolean("isLoading");
            if (!isLoading)
                loadFromRealm();
        } else {
            showProgressBar(true);
            downloadListTickets();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.ticket_list_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_created_des_list_menu:
                adapter.onSortTimeCreated(true);
                return true;
            case R.id.sort_created_asc_list_menu:
                adapter.onSortTimeCreated(false);
                return true;
            case R.id.sort_updated_des_list_menu:
                adapter.onSortTimeUpdated(true);
                return true;
            case R.id.sort_updated_asc_list_menu:
                adapter.onSortTimeUpdated(false);
                return true;
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(long id, String name) {
        Bundle bundle = new Bundle();
        bundle.putLong("idTicket", id);
        bundle.putLong("idProject", idProject);
        MainActivity.getInstance().showFragment(Consts.FragmentType.TICKET, bundle);
    }

    //----------- network methods --------------
    void downloadListTickets() {
        isLoading = true;
        Call<TicketListModel> call = ubugtrackApi.getTicketsList(String.valueOf(idProject), getQueryMapRequest());
        call.enqueue(new Callback<TicketListModel>() {
            @Override
            public void onResponse(Call<TicketListModel> call, final Response<TicketListModel> response) {
                isLoading = false;
                showProgressBar(false);
                EventBus.getDefault().postSticky(response.body());
            }
            @Override
            public void onFailure(Call<TicketListModel> call, Throwable t) {
                isLoading = false;
                showProgressBar(false);
                Log.e(Consts.TAG, "onFailure " + t.toString());
            }
        });
    }

    Map<String, String> getQueryMapRequest() {
        HashMap<String, String> map = new HashMap<>();
        if (filterStatus!=-1) {
            map.put("filter_status", String.valueOf(filterStatus));
            //map.put("result_start", "1");
            //map.put("result_limit", "100");
        }
        return map;
    }

    //----------- eventbus methods ----------
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onResponseSuccess(TicketListModel data) {
        List<TicketModel> list = new ArrayList<>();
        for (TicketModel ticketModel : data.ticketsList)
            list.add(new TicketModel(ticketModel));
        if (createdTicket!=null) {
            TicketModel newTicket = adapter.onAddNewTicket(list, createdTicket);
            createdTicket = null;
            if (newTicket!=null)
                saveInRealmNewTicket(newTicket);
        } else {
            adapter.onAddAll(list);
            saveInRealm(list);
        }
        EventBus.getDefault().removeStickyEvent(data);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUpdateTicket(TicketUpdateModel model) {
        if (model.id==-1) {
            createdTicket = model;
            filterStatus = model.status;
            downloadListTickets();
        } else
            adapter.onUpdateTicket(model);
        EventBus.getDefault().removeStickyEvent(model);
    }

    //----------- storage methods -----------
    void saveInRealm(List<TicketModel> list) {
        super.onSaveInRealm();
        realm.beginTransaction();
        realm.delete(TicketModel.class);
        realm.copyToRealmOrUpdate(list);
        realm.commitTransaction();
        realm.close();
    }

    void saveInRealmNewTicket(TicketModel ticket) {
        super.onSaveInRealm();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(ticket);
        realm.commitTransaction();
        realm.close();
    }

    void loadFromRealm() {
        Log.i(Consts.TAG, "loadFromRealm");
        List<TicketModel> list = realm.where(TicketModel.class).findAll();
        adapter.onAddAll(list);
    }

    //----------- Test data ------------
    ArrayList<TicketModel> getTestData() {
        ArrayList<TicketModel> list = new ArrayList<>();
        list.add(new TicketModel(1,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 0, 0, "external", "Assigned", true));
        list.add(new TicketModel(2,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 1, 1, "external", "Assigned", true));
        list.add(new TicketModel(3,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 2, 2, "external", "Assigned", true));
        list.add(new TicketModel(4,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 3, 3, "external", "Assigned", true));
        list.add(new TicketModel(5,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 4, 0, "external", "Assigned", true));
        list.add(new TicketModel(6,"2016-12-01","2016-12-02","Какой-то странный баг", "Здесь описание", 0, 1, "external", "Assigned", true));
        return list;
    }
}
