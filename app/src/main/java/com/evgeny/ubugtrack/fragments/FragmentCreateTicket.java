package com.evgeny.ubugtrack.fragments;

import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.evgeny.ubugtrack.MainActivity;
import com.evgeny.ubugtrack.R;
import com.evgeny.ubugtrack.databinding.FragmentTicketCreateBinding;
import com.evgeny.ubugtrack.models.ResponseModel;
import com.evgeny.ubugtrack.models.TicketUpdateModel;
import com.evgeny.ubugtrack.network.ApiFactory;
import com.evgeny.ubugtrack.network.UbugtrackApi;
import com.evgeny.ubugtrack.utils.Consts;
import com.evgeny.ubugtrack.views.menu.PriorityPopupMenu;
import com.evgeny.ubugtrack.views.menu.StatusPopupMenu;
import com.shehabic.droppy.DroppyClickCallbackInterface;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by estrelnikov on 11.01.2017.
 */

public class FragmentCreateTicket extends Fragment {
    FragmentTicketCreateBinding binding;
    PriorityPopupMenu priorityPopupMenu;
    StatusPopupMenu statusMenu;
    Button createNewTicketButton;
    UbugtrackApi ubugtrackApi;

    long idProject;
    boolean isEditState = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ubugtrackApi = ApiFactory.getUbugtrackApi();
        Bundle bundle = getArguments();
        if (bundle!=null) {
            idProject = bundle.getLong("idProject");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ticket_create, container, false);
        View rootView = binding.getRoot();
        initViews(rootView);
        return rootView;
    }

    void initViews(View view) {
        setHasOptionsMenu(true);

        createNewTicketButton = (Button)view.findViewById(R.id.createButton);
        createNewTicketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validNewTicket())
                    createNewTicket();
                else
                    Toast.makeText(MainActivity.getInstance(), MainActivity.getInstance().getResources().getString(R.string.title_null), Toast.LENGTH_LONG).show();
            }
        });
        statusMenu = new StatusPopupMenu(getActivity(), binding.statusTicketCreate, new DroppyClickCallbackInterface() {
            @Override
            public void call(View v, int id) {
                int[] colorArray = v.getContext().getResources().getIntArray(R.array.status_color_array);
                String[] str = v.getContext().getResources().getStringArray(R.array.status_text_array);
                binding.statusTicketCreate.setLabelText(str[id]);
                binding.statusTicketCreate.setBackgroundColor(colorArray[id]);
                binding.statusTicketCreate.setIndex(id);
            }
        });
        priorityPopupMenu = new PriorityPopupMenu(getActivity(), binding.priorityTicketCreate, new DroppyClickCallbackInterface() {
            @Override
            public void call(View v, int id) {
                String[] str = v.getContext().getResources().getStringArray(R.array.priority_text_array);
                binding.priorityTicketCreate.setLabelText(str[id]);
                binding.priorityTicketCreate.setIndex(id);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void changeEditable() {
        isEditState = !isEditState;
        // кнопки сохранение и изменения
        createNewTicketButton.setVisibility(isEditState?View.VISIBLE:View.GONE);
        // поля
        binding.titleTicketCreate.setEnabled(isEditState);
        binding.descriptionTicketCreate.setEnabled(isEditState);
        binding.priorityTicketCreate.setClickable(isEditState);
        binding.statusTicketCreate.setClickable(isEditState);
    }

    //------------ networks --------------
    void createNewTicket() {
        Call<ResponseModel> ticketUpdate = ubugtrackApi.createNewTicket(String.valueOf(idProject), getNewTicket());
        ticketUpdate.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.body().success) {
                    Toast.makeText(MainActivity.getInstance(), MainActivity.getInstance().getResources().getString(R.string.ticket_created), Toast.LENGTH_LONG).show();
                    changeEditable();
                    EventBus.getDefault().postSticky(getNewTicket());
                }
            }
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e(Consts.TAG, "onFailure " + t.toString());
            }
        });
    }

    //------------- GET-SET methods --------------
    TicketUpdateModel getNewTicket() {
        TicketUpdateModel updateModel = new TicketUpdateModel();
        updateModel.title = binding.titleTicketCreate.getText().toString();
        updateModel.description = binding.descriptionTicketCreate.getText().toString();
        updateModel.priority = binding.priorityTicketCreate.getIndex();
        updateModel.status = binding.statusTicketCreate.getIndex();
        return updateModel;
    }

    boolean validNewTicket() {
        if (binding.titleTicketCreate.getText().toString().length()>0)
            return true;
        return false;
    }
}
