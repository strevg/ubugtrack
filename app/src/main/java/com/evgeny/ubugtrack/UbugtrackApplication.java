package com.evgeny.ubugtrack;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.evgeny.ubugtrack.utils.Consts;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by estrelnikov on 21.02.2017.
 */

public class UbugtrackApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // load key api
        loadKeyAPI();

        // initialisation Realm
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }

    void loadKeyAPI() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Consts.API_KEY = sharedPreferences.getString(Consts.API_KEY_STR, "");
        Consts.HAS_API_KEY = sharedPreferences.getBoolean(Consts.HAS_API_KEY_STR, false);
    }
}
