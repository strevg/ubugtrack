package com.evgeny.ubugtrack.utils;

import android.content.Context;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by estrelnikov on 28.11.2016.
 */

public class StringHelper {
    public static String getDateByUnit(String unitTime) {
        Date time = new Date(Long.valueOf(unitTime)*1000);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time).toString();
    }

    public static String getCreatedDateString(String date) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(date.substring(0,4)).append("-");
        buffer.append(date.substring(4,6)).append("-");
        buffer.append(date.substring(6));
        return buffer.toString();
    }

    public static Date getUpdatedDate(String unitTime) {
        return new Date(Long.valueOf(unitTime)*1000);
    }

    public static Date getCreatedDate(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date created = new Date();
        try {
            created = format.parse(date);
        } catch (Exception e) {
            Log.e(Consts.TAG, "getCreatedDate " + e.toString());
        }
        return created;
    }
}
