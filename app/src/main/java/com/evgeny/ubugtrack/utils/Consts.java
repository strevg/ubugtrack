package com.evgeny.ubugtrack.utils;

import com.evgeny.ubugtrack.models.TicketModel;

/**
 * Created by Evgeny on 27.11.2016.
 */

public class Consts {
    public static String TAG = "ubugtrack";

    public static boolean TEST_DATA = true;
    public static boolean HAS_API_KEY = false;
    public static String API_KEY;// = "6b285a606bf21bedd4433d5f8915f89853442843ed8fc9e69e1c14bd306fc21a";
    public static String API_KEY_STR = "API_KEY_STR";
    public static String HAS_API_KEY_STR = "HAS_API_KEY";

    public enum FragmentType {PROJECTS_LIST, TICKETS_LIST, TICKET, CREATE_TICKET, LOGIN};
}
